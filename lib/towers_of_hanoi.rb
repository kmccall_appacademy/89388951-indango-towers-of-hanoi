# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.
class TowersOfHanoi
  attr_reader :towers

  def initialize
    @towers = [[4,3,2,1], [], []]
  end

  def play
    display

    until won? == true
      puts "What tower would you like to move from?"
      from_tower = (gets.to_i - 1)

      puts "What tower would you like to move to?"
      to_tower = (gets.to_i - 1)

      if valid_move?(from_tower, to_tower)
        move(from_tower, to_tower)
        display
      else
        display
        puts "Invalid Move! Please try again!"
      end
    end
    puts "You win!"
  end

  def render
    towers.each_index do |i|
      puts "Tower #{i + 1}: " + @towers[i].join(' ') + "\n"
    end
  end

  def won?
    @towers[0].empty? && @towers[1..-1].one? { |field| !field.empty? }
  end

  def move(from_tower, to_tower)
    @towers[to_tower] << @towers[from_tower].pop
  end

  def valid_move?(from_tower, to_tower)
    from = @towers[from_tower]
    to = @towers[to_tower]

    return false unless [from_tower, to_tower].all? { |i| i.between?(0, 2) }

    !from.empty? && (to.empty? || to.last > from.last)
  end

  def display
    system('clear')
    render
  end
end

  if __FILE__ == $PROGRAM_NAME
  TowersOfHanoi.new().play
  end
